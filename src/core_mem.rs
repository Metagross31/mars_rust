use crate::constants::{CORE_SIZE, MAX_TURNS};
use crate::Program;
use rand::Rng;

pub struct Core {
    memory: Vec<usize>,
    indices: [usize; 2],
}

impl Core {
    pub fn new() -> Core {
        Core {
            memory: vec![0; CORE_SIZE],
            indices: [0; 2],
        }
    }
    pub fn load_programs(&mut self, programs: (Program, Program)) {
        let (x_size, y_size) = (programs.0.instructions.len(), programs.1.instructions.len());
        assert!(
            x_size + y_size <= CORE_SIZE,
            "The chosen core size is too small to hold the programs!"
        );
        let mut rng = rand::thread_rng();
        self.indices = [0, x_size + rng.gen_range(0..(CORE_SIZE - x_size - y_size))];
        let (x_index, y_index) = (self.indices[0], self.indices[1]);

        for i in x_index..(x_index + x_size) {
            self.memory[i] = programs.0.instructions[i];
        }
        for i in y_index..(y_index + y_size) {
            self.memory[i] = programs.1.instructions[i];
        }
    }

    pub fn run_corewar(&mut self) -> Result<(), CoreWarResult> {
        for _ in 0..MAX_TURNS {
            let x_instruction = self.memory[self.indices[0]];
            self.execute_instruction(x_instruction, 0)?;
            let y_instruction = self.memory[self.indices[1]];
            self.execute_instruction(y_instruction, 1)?;
        }
        Err(CoreWarResult::Draw)
    }

    fn execute_instruction(
        &mut self,
        instruction: usize,
        player: usize,
    ) -> Result<(), CoreWarResult> {
        let instruction_type = instruction / u32::pow(2, 28) as usize;
        let mode_a = (instruction / u32::pow(2, 26) as usize) % 4;
        let mode_b = (instruction / u32::pow(2, 24) as usize) % 4;
        let field_a = (instruction / u32::pow(2, 12) as usize) % u32::pow(2, 12) as usize;
        let field_b = instruction % u32::pow(2, 12) as usize;
        let operand_a;
        let operand_b;
        let mut address_a = 0;
        let mut address_b = 0;
        let pointer;

        match mode_a {
            0 => {
                operand_a = field_a;
            }
            1 => {
                address_a = (self.indices[player] + field_a) % CORE_SIZE;
                operand_a = self.memory[address_a];
            }
            2 => {
                pointer = (self.indices[player] + field_a) % CORE_SIZE;
                address_a = (pointer + self.memory[pointer]) % CORE_SIZE;
                operand_a = self.memory[address_a];
            }
            _ => return Err(CoreWarResult::Loss(player)),
        }

        let pointer;

        match mode_b {
            0 => {
                operand_b = field_b;
            }
            1 => {
                address_b = (self.indices[player] + field_b) % CORE_SIZE;
                operand_b = self.memory[address_b];
            }
            2 => {
                pointer = (self.indices[player] + field_b) % CORE_SIZE;
                address_b = (pointer + self.memory[pointer]) % CORE_SIZE;
                operand_b = self.memory[address_b];
            }
            _ => return Err(CoreWarResult::Loss(player)),
        }
        let address_a = address_a;
        let address_b = address_b;

        self.indices[player] += 1;

        let answer;
        match instruction_type {
            0 => return Err(CoreWarResult::Loss(player)),
            1 => self.memory[address_b] = operand_a,
            2 => {
                answer = operand_a + operand_b;
                self.memory[address_b] = answer;
            }
            3 => {
                answer = operand_b - operand_a;
                self.memory[address_b] = answer;
            }
            4 => {
                self.indices[player] = operand_b % CORE_SIZE;
            }
            5 => {
                if operand_a == 0 {
                    self.indices[player] = operand_b;
                }
            }
            6 => {
                answer = operand_a - 1;
                self.memory[address_a] = answer;
                if answer == 0 {
                    self.indices[player] = operand_b;
                }
            }
            7 => {
                if operand_a == operand_b {
                    self.indices[player] += 1;
                }
            }
            _ => return Err(CoreWarResult::Loss(player)),
        }
        Ok(())
    }
}

pub enum CoreWarResult {
    Loss(usize),
    Draw,
}
