use crate::core_mem::{Core, CoreWarResult};
use std::env;

mod constants;
mod core_mem;
mod programs;

use crate::programs::Program;

fn main() {
    assert!(
        env::args().len() == 3,
        "Less then two programs were specified!"
    );

    let filenames: Vec<String> = env::args().collect();

    let program1 = Program::from_file(&filenames[1]);
    let program2 = Program::from_file(&filenames[2]);
    let mut core = Core::new();
    core.load_programs((program1, program2));
    match core.run_corewar() {
        Ok(()) => println!("No contest!"),
        Err(CoreWarResult::Draw) => println!("Draw!"),
        Err(CoreWarResult::Loss(p)) => println!("Player {} won the CoreWar!", 1 - p + 1),
    }
}
